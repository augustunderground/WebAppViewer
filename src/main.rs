extern crate web_view;

use web_view::*;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    let url = if args.len() < 2 { "https://github.com/Boscop/web-view"} 
              else { &args[1] };
    
    let title = if args.len() < 3 { "Rust Web Viewer" }
                else { &args[2] };

    web_view::builder()
             .title(title)
             .content(Content::Url(url))
             .size(720, 1440)
             .frameless(true)
             .debug(true)
             .user_data(())
             .invoke_handler(|_webview, _arg| Ok(()))
             .run()
             .unwrap();
}
