# Viewer for WebApps

Based on [web-view](https://github.com/Boscop/web-view) 
for [Rust](https://www.rust-lang.org/).

Intended for use with the [PinePhone](https://www.pine64.org/pinephone/) 
/ [Phosh](https://developer.puri.sm/Librem5/Software_Reference/Environments/Phosh.html).

## Build

```bash
$ cargo build
```

## Install

Put `./target/debug/webviewer` somewhere in your `$PATH`.

```bash
$ sudo cp -v ./target/debug/webviewer /usr/bin/
```

## Apps

See `examples` for how to use this with websites/SPAs.

### Home Screen Icon

Create a script launching `webviewer` with the `URL` of
the page/SPA you want.

```bash
webviewer https://protonmail.com Protonmail
```

Where the first argument has to be the URL and the second argument is the Title
of the resulting window.

Create `desktop` entry:

```ini
[Desktop Entry]
Version=0.1
Name=Protonmail
Comment=Protonmail Client
Exec=protonmail.sh
Icon=protonmail.png
Terminal=false
Type=Application
```

Where `Exec` points to a specific path if the shell wrapper is not in your `$PATH`,
and `Icon` points to a specific path, where the corresponding icon is located.
See the [gnome documentation](https://developer.gnome.org/integration-guide/stable/desktop-files.html.en)
on this topic for exact instructions.

## What doesn't work

- [ ] Links that open new windows
